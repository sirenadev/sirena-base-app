import React from 'react'
import logo from './logo.svg'
import OctoBat from './OctoBat.svg'
import './App.css'
import { BrowserRouter as Router } from 'react-router-dom'

function App () {
  return (
    <div className="App">
      <Router basename={'/client'}>
        <header className="App-header">
          <img src={OctoBat} className="App-logo" alt="logo" />
          <p>
            Sirena Base App
      </p>
          <a
            className="App-link"
            href="https://reactjs.org"
            target="_blank"
            rel="noopener noreferrer"
          >
            Learn React
      </a>
          <br />
          <a
            className="App-link"
            href="http://dev-sirena-docs.s3-website-us-east-1.amazonaws.com/earth-common-docs"
            target="_blank"
            rel="noopener noreferrer"
          >
            Sirena Styles & Guidelines
      </a>
        </header>

      </Router>
    </div>
  )
}

export default App

# Sirena Base App

Client + Server App built with Mongo, Koa, React.js, Node.js
## Table of Contents

- [Installation](#installation)
- [Usage](#usage)
- [Basic structure](#basic-structure)
- [Styles & Guidelines](#styles-and-guidelines)
- [ToDo's](#todos)

## Installation

##### Use [npm](https://docs.npmjs.com/downloading-and-installing-node-js-and-npm) to install all dependencies from **Client** and **Server**.

```bash
npm run install
```

##### Start the app using

```bash
npm run local
```

---

#### Client App will be mounted on `http://localhost:3000/client`

#### Server App will be mounted on `http://localhost:9000/api`

---

### Basic structure

```
      server.js
      package.json         // server deps and scripts
      src/
          errors/
          models/
          services/
          public/
          ...
      config/
            default.yml
            ...
      client/
          src/
              App.js
              index.js
              App.css
              ...
              package.json         // client deps and scripts
        .../
        .../
```


## Styles and Guidelines


> Module used:  [@sirena/earth-common](https://www.npmjs.com/package/@sirena/earth-common)

> [Documentation Here](http://dev-sirena-docs.s3-website-us-east-1.amazonaws.com/earth-common-docs/)

## IMPORTANT MATTER REGARDING MATERIAL-UI (MUI) AND @SIRENA/EARTH-COMMON

@sirena/earth-common uses `@material-ui/core": "3.9.2"`, the latest version from v3.x.x

If you want to install MUI from the [official site](https://material-ui.com/getting-started/installation/), please ensure you are using the same version and use the docs from that version --> https://material-ui.com/getting-started/installation/

---
### Todos

- Add docs regarding Integration and their types
- Add SDK to connect to Sirena API
- Linter
- Sections
  - Deployment
  - Testing

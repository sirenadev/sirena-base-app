"use strict";

const Integration = require("../../models/Integration");
const { providerForm } = require("../../utils/defaults");

module.exports = async function getForm(apiKey) {
  const form = JSON.parse(JSON.stringify(providerForm));
  if (apiKey) {
    const integration = await Integration.findOne({ apiKey });
    if (!integration) {
      const error = new Error(`The integration ${apiKey} does not exists.`);
      error.status = 404;
      throw error;
    }
    // Populate your form here for the provider apikey
  }
  return { body: form, status: 200 };
};

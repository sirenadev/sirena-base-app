'use strict'
const Integration = require('../../models/Integration')

module.exports = async function updateApiKey(apiKey, newApiKey) {
  const integration = await Integration.findOne({ apiKey })
  if (newApiKey && newApiKey !== '') {
    if (!integration) {
      const error = new Error(`Not found integration "${apiKey}"`)
      error.status = 404
      throw error
    }
    await integration.set('apiKey', newApiKey)
    await integration.save()
    return { body: { status: 'updated' }, status: 200 }
  } else {
    const error = new Error('apiKey invalid')
    error.status = 404
    throw error
  }
}

'use strict'

const Integration = require('../../models/Integration')

module.exports = async function setSettings(apiKey, settings) {
  const integration = await Integration.findOne({ apiKey })
  if (!integration) {
    const error = new Error(`Not found integration "${apiKey}"`)
    error.status = 404
    throw error
  }
  const newSettings = { ...integration.settings, ...settings }
  await integration.set('settings', newSettings)
  await integration.save()
  return { body: { status: 'updated' }, status: 200 }
}


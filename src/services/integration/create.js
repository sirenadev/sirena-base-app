'use strict'
const Integration = require('../../models/Integration')

module.exports = async function create(integration) {
  const integrationData = {
    apiKey: integration.apiKey,
    providerKey: integration.providerKey,
    enabled: integration.enabled,
    softRemoved: false,
    settings: integration.settings,
    webHookSubscriptions: integration.webHookSubscriptions
  }
  await Integration.create(integrationData)
  return {
    body: { status: 'created' },
    status: 201
  }
}

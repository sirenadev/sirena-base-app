'use strict'

const Integration = require('../../models/Integration')
const { providerSettings } = require('../../utils/defaults')

module.exports = async function getSettings(apiKey) {
	if (!apiKey) {
		throw new Error('ApiKey is required')
	}
	try {
		const integrationSettings = await Integration.findOne({
			apiKey,
			enabled: true,
			softRemoved: false,
		})
			.select('settings')
			.lean()
			.exec()

		if (!integrationSettings) {
			const error = new Error(`Not found integration "${apiKey}"`)
			error.status = 404
			throw error
		}
		const settings = { ...providerSettings, ...integrationSettings }
		return { body: settings, status: 200 }
	} catch (e) {
		const error = new Error(
			`The integration with apiKey ${apiKey} does not exists.`
		)
		error.status = 404
		throw error
	}
}

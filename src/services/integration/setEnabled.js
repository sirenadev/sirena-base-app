'use strict'
const Integration = require('../../models/Integration')

module.exports = async function setEnabled (apiKey, enabled) {
  const integration = await Integration.findOne({ apiKey })
  if (!integration) {
    const error = new Error(`Not found integration "${apiKey}"`)
    error.status = 404
    throw error
  }
  await integration.set('enabled', enabled)
  await integration.save()
  return { body: { enabled }, status: 200 }
}

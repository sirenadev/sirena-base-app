'use strict'

const Integration = require('../../models/Integration')

module.exports = async function deleteIntegration (apiKey) {
  const integration = await Integration.findOne({ apiKey }).lean().exec()
  if (!integration) {
    const error = new Error(`Not found integration "${apiKey}"`)
    error.status = 404
    throw error
  }
  await Integration.updateOne(
    { apiKey },
    { softRemoved: true, enabled: false }
  ).exec()

  return { body: { status: 'deleted' }, status: 200 }
}

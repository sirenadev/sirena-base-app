'use strict'

const Integration = require('../../models/Integration')

module.exports = async function get(sirenaAppId) {
  try {
    const integration = await Integration.findOne({
      'settings.sirenaAppId': sirenaAppId
    })
      .lean()
      .exec()
    if (!integration) {
      const error = new Error(`Not found integration "${sirenaAppId}"`)
      error.status = 404
      throw error
    }
    return integration
  } catch (e) {
    const error = new Error(`Error ${e}`)
    error.status = 400
    throw error
  }
}

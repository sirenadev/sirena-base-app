'use strict'

const config = require('../../init/config')()
const integrationServices = require('../../services/integration')

module.exports = {
	async getForm(ctx) {
		// Here, you can specify a JSON with data you want to send to Sirena in order to render your form.
		// With this form, people will be able to set specific information, like an email, url, etc.
		const apiKey = ctx.request.query.apiKey
		const response = await integrationServices.getForm(apiKey)
		ctx.body = response.body
		ctx.status = response.status
	},
	async getSettings(ctx) {
		// You can obtain the current settings for your integration/app
		const apiKey = ctx.request.query.apiKey
		const response = await integrationServices.getSettings(apiKey)
		ctx.body = response.body
		ctx.status = response.status
	},
	async setSettings(ctx) {
		// You can use this to persist data that was retrieved from the form in your integration/app
		const settings = ctx.request.body.settings
		if (!settings) {
			const error = new Error('Cannot update integration without settings')
			error.status = 400
			throw error
		}
		const apiKey = ctx.request.query.apiKey
		if (!apiKey) {
			const error = new Error('Cannot find integration without apiKey')
			error.status = 400
			throw error
		}
		const response = await integrationServices.setSettings(apiKey, settings)
		ctx.body = response.body
		ctx.status = response.status
	},
	async getStatus(ctx) {
		// Return the status of your app, whether an error exists or not. By default, is set to return an OK status
		ctx.body = { statusType: 'OK' }
		ctx.status = 200
	},
	async setEnabled(ctx) {
		// Use this to enable/disable an integration/app
		const apiKey = ctx.request.query.apiKey
		const enabled = ctx.request.body.enabled
		if (!apiKey) {
			const error = new Error('Cannot find integration without apiKey')
			error.status = 400
			throw error
		}
		if (typeof enabled === 'undefined') {
			const error = new Error('Missing required argument -> enabled')
			error.status = 400
			throw error
		}
		const response = await integrationServices.setEnabled(apiKey, enabled)
		ctx.body = response.body
		ctx.status = response.status
	},
	async updateApiKey(ctx) {
		// Use this in order to update the apiKey of your integration/app
		const apiKey = ctx.request.body.apiKey
		const newApiKey = ctx.request.body.newApiKey
		if (!apiKey) {
			const error = new Error('Cannot find integration without apiKey')
			error.status = 400
			throw error
		}
		if (!newApiKey) {
			const error = new Error('Missing required argument -> newApiKey')
			error.status = 400
			throw error
		}
		const response = await integrationServices.updateApiKey(apiKey, newApiKey)
		ctx.body = response.body
		ctx.status = response.status
	},
	async createIntegration(ctx) {
		// Use this in order to create an integration/app
		const integrationData = ctx.request.body.integration
		const appId = ctx.request.body.appId
		const appSecret = ctx.request.body.appSecret
		if (!appId || !appSecret) {
			const error = new Error('Cannot create quote without credentials')
			error.status = 400
			throw error
		}
		if (!integrationData) {
			const error = new Error(
				'Cannot create integration without integrationData'
			)
			error.status = 400
			throw error
		}
		if (!integrationData.apiKey) {
			const error = new Error('Cannot create integration without apiKey')
			error.status = 400
			throw error
		}

		if (appSecret !== config.appSecret || appId !== config.appId) {
			const error = new Error('Invalid credentials')
			error.status = 401
			throw error
		}

		const response = await integrationServices.create(integrationData)
		ctx.body = response.body
		ctx.status = response.status
	},
	async deleteIntegration(ctx) {
		// Use this in order to delete your integration/app
		const apiKey = ctx.request.query.apiKey
		if (!apiKey) {
			const error = new Error('Cannot find integration without apiKey')
			error.status = 400
			throw error
		}
		const response = await integrationServices.delete(apiKey)
		ctx.body = response.body
		ctx.status = response.status
	},
}

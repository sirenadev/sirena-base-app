const crypto = require('crypto')
const get = require('../../services/integration/get')

const signedUrlValidation = async function(ctx, next) {
	const { url, signature, sirenaAppId } = ctx.query
	const {
		settings: { privateKey },
	} = await get(sirenaAppId)
	const REPLACED_URL = url.replace(/(\?|&)signature=.+/, '')
	const hmac = crypto.createHmac('sha256', privateKey)
	hmac.update(REPLACED_URL)
	const EXPECTED_SIGNATURE = hmac.digest('hex')
	if (signature !== EXPECTED_SIGNATURE) {
		const error = new Error('Unauthorized request')
		error.status = 403
		throw error
	} else {
		await next()
	}
}

module.exports = {
	signedUrlValidation,
}

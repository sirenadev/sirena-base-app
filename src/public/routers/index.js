'use strict'

/**
 * Init routes
 */
const Router = require('koa-router')
const router = new Router()

router.prefix('/integration')

const apiController = require('../api')

// Add here all the controllers you have in your app


// Get the default config of a provider or the integration settings
router.get('/settings', apiController.getSettings)

// Set new settings for a created integration
router.put('/settings', apiController.setSettings)

// Get the form
router.get('/form', apiController.getForm)

// Get provider status or integration status
router.get('/status', apiController.getStatus)

// Enable/disable integration
router.put('/enabled', apiController.setEnabled)

// Update integration apiKey
router.put('/apiKey', apiController.updateApiKey)

// create a new integration
router.post('/create', apiController.createIntegration)

// delete one integration
router.delete('/delete', apiController.deleteIntegration)

module.exports = router

const mongoose = require('mongoose')
const config = require('./config')()
const logger = require('./logger')
const dbLogger = logger.child({ service: 'database' })
const connections = new Map()

function createConnection (connectionName, connectionString) {
  if (!connections.has(connectionString)) {
    const connectionOptions = {
      useNewUrlParser: true,
      appname: `${config.appName}`,
      useUnifiedTopology: true,
      poolSize: 20,
      useCreateIndex: true
    }

    const conn = mongoose.createConnection(connectionString, connectionOptions)

    conn
      .on('error', (err) => {
        dbLogger.error(
          `[Mongo] Connection error with ${connectionName}: ${err.message}`
        )
      })
      .on('open', () => {
        dbLogger.info(`[Mongo] Connected to ${connectionName}`)
      })
      .on('close', () => {
        dbLogger.info(`[Mongo] Closed connection to ${connectionName}`)
      })
      .on('connecting', () => {
        dbLogger.info(`[Mongo] Connecting to ${connectionName}`)
      })
      .on('disconnecting', () => {
        dbLogger.info(`[Mongo] Disconnecting from ${connectionName}`)
      })
      .on('disconnected', () => {
        dbLogger.info(`[Mongo] Disconnected from ${connectionName}`)
      })
      .on('reconnected', () => {
        dbLogger.info(`[Mongo] Reconnected to ${connectionName}`)
      })
      .on('disconnected', () => {
        dbLogger.error(`[Mongo] Disconnected from ${connectionName}`)
      })

    connections.set(connectionString, conn)
  }

  return connections.get(connectionString)
}

const appDB =
  config.environment === 'testing'
    ? mongoose.connections[0]
    : createConnection(`${config.appName} DB`, config.appDB)

module.exports = {
  appDB
}

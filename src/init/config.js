const nconf = require('nconf')
const { safeDump, safeLoad } = require('js-yaml')
const { accessSync } = require('fs')
const { join } = require('path')

const configRoot = 'config'

module.exports = function readConfig() {
  const defaultConfigPath = join(configRoot, 'default.yml')

  try {
    accessSync(defaultConfigPath)
  } catch (e) {
    // default config file is not accessible, throw an error
    throw new Error(
      'Default configuration file not found or not accessible. Check the config directory.'
    )
  }

  const environment = process.env.NODE_ENV || 'local'

  // environment-specific config
  const envConfigPath = join(configRoot, `${environment}.yml`)

  // use yaml config so we can use comments and other nice features
  const yamlFormat = {
    parse: safeLoad,
    stringify: safeDump
  }

  // set the configuration hierarchy
  nconf
    .argv()
    .env()
    .file('fileEnv', {
      file: envConfigPath,
      format: yamlFormat
    })
    .file('fileDefault', {
      file: defaultConfigPath,
      format: yamlFormat
    })

  return {
    appName: 'App Name',
    environment: environment,
    port: Number(nconf.get('PORT')),
    appDB: nconf.get('MONGO'),
    sirenaEndpoint: nconf.get('SIRENA_ENDPOINT'),
    self: nconf.get('SELF'),
    isProduction: environment === 'production',
    appId: nconf.get('APP_ID'),
    appSecret: nconf.get('APP_SECRET'),
    clientUrl: nconf.get('CLIENT')
  }
}

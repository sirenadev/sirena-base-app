'use strict'

const mongoose = require('mongoose')
const uniqueValidator = require('mongoose-unique-validator')
const { appDB } = require('../init/database')

/**
 * Schema definition
 */
const schema = new mongoose.Schema(
	{
		apiKey: {
			type: String,
			required: true,
			unique: true,
		},
		providerKey: {
			type: String,
			required: true,
		},
		settings: {
			type: Object,
			default: {},
		},
		webHookSubscriptions: {
			type: Object,
			default: {},
		},
		enabled: {
			type: Boolean,
			default: true,
		},
		softRemoved: {
			type: Boolean,
			default: false,
		},
		pending: {
			type: Boolean,
		},
		createdAt: {
			type: Date,
			default: Date.now,
		},
		updatedAt: Date,
	},
	{
		collection: 'integrations',
		timestamps: true,
	}
)

schema.plugin(uniqueValidator)

schema.methods.toPublicObject = function toPublicObject(ext) {
	const lean = this.toObject()
	lean.id = lean._id.toHexString()
	lean.enabled = Boolean(lean.enabled)
	lean.pending = Boolean(lean.pending)

	if (lean.providerKey) {
		const dotAt = lean.providerKey.indexOf('.')
		const provider =
			(dotAt > 0 && lean.providerKey.slice(dotAt)) || lean.providerKey
		lean.provider = provider
	}

	return Object.assign(lean, ext)
}

module.exports = appDB.model('integration', schema)

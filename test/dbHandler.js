'use strict'

const mongoose = require('mongoose')
const { MongoMemoryServer } = require('mongodb-memory-server')
const mongoTest = new MongoMemoryServer({ instance: { dbName: 'test_bd' } })

/**
 * Connect to the in-memory database
 */
module.exports.connect = async () => {
  // Get the uri
  const uri = await mongoTest.getUri()
  // Configure the database options
  const mongooseOpts = {
    useNewUrlParser: true,
    autoReconnect: true,
    reconnectTries: Number.MAX_VALUE,
    reconnectInterval: 1000
  }
  // Generate the connection
  await mongoose.connect(uri, mongooseOpts)
}

module.exports.closeDatabase = async () => {
  await mongoose.connection.dropDatabase()
  await mongoose.connection.close()
  await mongoTest.stop()
}

module.exports.clearDatabase = async () => {
  const collections = mongoose.connection.collections
  for (const key in collections) {
    const collection = collections[key]
    await collection.deleteMany()
  }
}

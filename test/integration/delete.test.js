const create = require('../../src/services/integration/create')
const deleteIntegration = require('../../src/services/integration/delete')
const get = require('../../src/services/integration/get')
const dbHandler = require('../dbHandler')

let integrationTest = {}

//validate connection  to in memory mongo server
beforeAll(async () => await dbHandler.connect())

beforeEach(async () => {
  integrationTest = {
    apiKey: `5RCiXZOUYz1yOSL6Ye3WFDD9m59cXfjmfc${Math.random()
      .toString(36)
      .substr(2, 9)}`,
    providerKey: 'test-provider',
    settings: {
      sirenaAppId: `7TYiXZOUYz1yOSL6Ye3WFDD9m59cXfsEr${Math.random()
        .toString(36)
        .substr(2, 9)}`,
    },
  }
  await create(integrationTest)
})

//clear all collection in database
afterEach(async () => await dbHandler.clearDatabase())

//close connection to database
afterAll(async () => await dbHandler.closeDatabase())

describe('Delete the integration', () => {
  test('Should delete the integration if the integration exists', async () => {
    const { apiKey } = integrationTest
    const response = await deleteIntegration(apiKey)
    expect(response.status).toBe(200)
  })
  test('Should throw an error if the integration doesnt exists', async () => {
    const { apiKey } = integrationTest
    expect(async () => await deleteIntegration(`${apiKey}s`)).rejects.toThrow()
  })
  test('In the database, should be enabled false and softRemoved in true', async () => {
    const {
      apiKey,
      settings: { sirenaAppId },
    } = integrationTest
    await deleteIntegration(apiKey)
    const integration = await get(sirenaAppId)
    expect(integration.softRemoved).toBeTruthy()
    expect(integration.enabled).toBeFalsy()
  })
})

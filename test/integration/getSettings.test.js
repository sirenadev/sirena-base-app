const create = require('../../src/services/integration/create')
const get = require('../../src/services/integration/get')
const getSettings = require('../../src/services/integration/getSettings')
const dbHandler = require('../dbHandler')

let integrationTest = {}

//validate connection  to in memory mongo server
beforeAll(async () => await dbHandler.connect())

beforeEach(async () => {
  integrationTest = {
    apiKey: `5RCiXZOUYz1yOSL6Ye3WFDD9m59cXfjmfc`,
    providerKey: 'test-provider',
    settings: {
      sirenaAppId: `7TYiXZOUYz1yOSL6Ye3WFDD9m59cXfsEr`,
    },
  }
  await create(integrationTest)
})

//clear all collection in database
afterEach(async () => await dbHandler.clearDatabase())

//close connection to database
afterAll(async () => await dbHandler.closeDatabase())

describe('Get settings from integration', () => {
  test('Should get the settings if the integration exists', async () => {
    const { apiKey } = integrationTest
    const response = await getSettings(apiKey)
    expect(response.status).toBe(200)
  })
  test('Should throw an error if the integration doesnt exists', async () => {
    const { apiKey } = integrationTest
    await expect(getSettings(`${apiKey}s`)).rejects.toThrow()
  })
  test('Should get the same settings that when the integration was created', async () => {
    const { apiKey, settings: mockSettings } = integrationTest
    const {
      body: { settings },
    } = await getSettings(apiKey)
    expect(mockSettings).toEqual(settings)
  })
})

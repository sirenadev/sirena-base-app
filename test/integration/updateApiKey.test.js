const create = require('../../src/services/integration/create')
const get = require('../../src/services/integration/get')
const updateApiKey = require('../../src/services/integration/updateApiKey')
const dbHandler = require('../dbHandler')

let integrationTest = {}

//validate connection  to in memory mongo server
beforeAll(async () => await dbHandler.connect())

beforeEach(async () => {
  integrationTest = {
    apiKey: `5RCiXZOUYz1yOSL6Ye3WFDD9m59cXfjmfc${Math.random()
      .toString(36)
      .substr(2, 9)}`,
    providerKey: 'test-provider',
    settings: {
      sirenaAppId: `7TYiXZOUYz1yOSL6Ye3WFDD9m59cXfsEr${Math.random()
        .toString(36)
        .substr(2, 9)}`,
    },
    enabled: true,
  }
  await create(integrationTest)
})

//clear all collection in database
afterEach(async () => await dbHandler.clearDatabase())

//close connection to database
afterAll(async () => await dbHandler.closeDatabase())

describe('Update apikey', () => {
  test('Should update the apikey if the integration exists', async () => {
    const { apiKey } = integrationTest
    const newApiKey = `5RCiXZOUYz1yOSL6Ye3WFDD9m59cXfjmfc${Math.random()
      .toString(36)
      .substr(2, 9)}`
    const response = await updateApiKey(apiKey, newApiKey)
    expect(response.status).toBe(200)
  })
  test('Should throw an error if the integration doesnt exists', async () => {
    const { apiKey } = integrationTest
    const newApiKey = `5RCiXZOUYz1yOSL6Ye3WFDD9m59cXfjmfc${Math.random()
      .toString(36)
      .substr(2, 9)}`
    expect(
      async () => await updateApiKey(`${apiKey}s`, newApiKey)
    ).rejects.toThrow()
  })
  test('Should get integration with the new apiKey', async () => {
    const {
      apiKey,
      settings: { sirenaAppId },
    } = integrationTest
    const newApiKey = `5RCiXZOUYz1yOSL6Ye3WFDD9m59cXfjmfc${Math.random()
      .toString(36)
      .substr(2, 9)}`
    await updateApiKey(apiKey, newApiKey)
    const integration = await get(sirenaAppId)
    expect(integration.apiKey).toEqual(newApiKey)
  })
  test('If new apiKey is not valid, throw an error', async () => {
    const { apiKey } = integrationTest
    expect(async () => await updateApiKey(apiKey)).rejects.toThrow()
  })
})

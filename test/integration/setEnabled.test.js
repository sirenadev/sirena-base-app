const create = require('../../src/services/integration/create')
const get = require('../../src/services/integration/get')
const setEnabled = require('../../src/services/integration/setEnabled')
const dbHandler = require('../dbHandler')

let integrationTest = {}

//validate connection  to in memory mongo server
beforeAll(async () => await dbHandler.connect())

beforeEach(async () => {
  integrationTest = {
    apiKey: `5RCiXZOUYz1yOSL6Ye3WFDD9m59cXfjmfc${Math.random()
      .toString(36)
      .substr(2, 9)}`,
    providerKey: 'test-provider',
    settings: {
      sirenaAppId: `7TYiXZOUYz1yOSL6Ye3WFDD9m59cXfsEr${Math.random()
        .toString(36)
        .substr(2, 9)}`,
    },
    enabled: true,
  }
  await create(integrationTest)
})

//clear all collection in database
afterEach(async () => await dbHandler.clearDatabase())

//close connection to database
afterAll(async () => await dbHandler.closeDatabase())

describe('Set enabled from integration', () => {
  test('Should set the enabled value if the integration exists', async () => {
    const { apiKey, enabled } = integrationTest
    const response = await setEnabled(apiKey, enabled)
    expect(response.status).toBe(200)
  })
  test('Should throw an error if the integration doesnt exists', async () => {
    const { apiKey, enabled } = integrationTest
    expect(
      async () => await setEnabled(`${apiKey}s`, enabled)
    ).rejects.toThrow()
  })
  test('Should get the same enabled value that when the integration was created', async () => {
    const {
      apiKey,
      settings: { sirenaAppId },
      enabled,
    } = integrationTest
    await setEnabled(apiKey, enabled)
    const integration = await get(sirenaAppId)
    expect(integration.enabled).toEqual(enabled)
  })
  test('If enabled value is not especified, should set false by default', async () => {
    const {
      apiKey,
      settings: { sirenaAppId },
    } = integrationTest
    await setEnabled(apiKey)
    const integration = await get(sirenaAppId)
    expect(integration.enabled).toBeFalsy()
  })
})

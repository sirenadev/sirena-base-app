const create = require('../../src/services/integration/create')
const dbHandler = require('../dbHandler')

const integrationTest = {
  apiKey: `5RCiXZOUYz1yOSL6Ye3WFDD9m59cXfjmfc${Math.random()
    .toString(36)
    .substr(2, 9)}`,
  providerKey: 'test-provider',
}
const integrationBase = {
  apiKey: `5RCiXZOUYz1yOSL6Ye3WFDD9m59cXfsEr${Math.random()
    .toString(36)
    .substr(2, 9)}`,
  providerKey: 'test-provider-unique',
}

//validate connection  to in memory mongo server
beforeAll(async () => await dbHandler.connect())

//clear all collection in database
afterEach(async () => await dbHandler.clearDatabase())

//close connection to database
afterAll(async () => await dbHandler.closeDatabase())

describe('Create the integration', () => {
  test('Should create the integration', async () => {
    const response = await create(integrationTest)
    expect(response.status).toBe(201)
  })
  test('ApiKey should be unique', async () => {
    expect(async () => await create(integrationTest)).rejects.toThrow()
  })
  test('If integration object doesnt have required values, throw an error', async () => {
    const integrationFail = { ...integrationTest }
    integrationFail.apiKey = null
    expect(async () => await create(integrationTest)).rejects.toThrow()
  })
})

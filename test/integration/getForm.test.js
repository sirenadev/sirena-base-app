const { providerForm } = require('../../src/utils/defaults')
const create = require('../../src/services/integration/create')
const get = require('../../src/services/integration/get')
const getForm = require('../../src/services/integration/getForm')
const dbHandler = require('../dbHandler')

let integrationTest = {}

//validate connection  to in memory mongo server
beforeAll(async () => await dbHandler.connect())

beforeEach(async () => {
  integrationTest = {
    apiKey: `5RCiXZOUYz1yOSL6Ye3WFDD9m59cXfjmfc${Math.random()
      .toString(36)
      .substr(2, 9)}`,
    providerKey: 'test-provider',
    settings: {
      sirenaAppId: `7TYiXZOUYz1yOSL6Ye3WFDD9m59cXfsEr${Math.random()
        .toString(36)
        .substr(2, 9)}`,
    },
  }
  await create(integrationTest)
})

//clear all collection in database
afterEach(async () => await dbHandler.clearDatabase())

//close connection to database
afterAll(async () => await dbHandler.closeDatabase())

describe('Get form from integration', () => {
  test('Should get the form if the integration exists', async () => {
    const { apiKey } = integrationTest
    const response = await getForm(apiKey)
    expect(response.status).toBe(200)
  })
  test('Should get the provider form if apikey is null', async () => {
    const response = await getForm(null)
    expect(response.body).toEqual(providerForm)
  })
})

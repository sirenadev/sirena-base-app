const create = require('../../src/services/integration/create')
const get = require('../../src/services/integration/get')
const setSettings = require('../../src/services/integration/setSettings')
const dbHandler = require('../dbHandler')

let integrationTest = {}

//validate connection  to in memory mongo server
beforeAll(async () => await dbHandler.connect())

beforeEach(async () => {
  integrationTest = {
    apiKey: `5RCiXZOUYz1yOSL6Ye3WFDD9m59cXfjmfc${Math.random()
      .toString(36)
      .substr(2, 9)}`,
    providerKey: 'test-provider',
    settings: {
      sirenaAppId: `7TYiXZOUYz1yOSL6Ye3WFDD9m59cXfsEr${Math.random()
        .toString(36)
        .substr(2, 9)}`,
    },
    enabled: true,
  }
  await create(integrationTest)
})

//clear all collection in database
afterEach(async () => await dbHandler.clearDatabase())

//close connection to database
afterAll(async () => await dbHandler.closeDatabase())

describe('Set settings from integration', () => {
  test('Should set the settings if the integration exists', async () => {
    const { apiKey, settings } = integrationTest
    const response = await setSettings(apiKey, settings)
    expect(response.status).toBe(200)
  })
  test('Should throw an error if the integration doesnt exists', async () => {
    const { apiKey, settings } = integrationTest
    expect(
      async () => await setSettings(`${apiKey}s`, settings)
    ).rejects.toThrow()
  })
  test('Should get the settings integration and the new settings', async () => {
    const {
      apiKey,
      settings: { sirenaAppId },
      settings,
    } = integrationTest
    const newSettings = {
      group: `5c6eef17a04ab700635352ce${Math.random()
        .toString(36)
        .substr(2, 9)}`,
      privateKey: `TcUeq9NnGIOkoEQoPCTxh9hQsT65HyZQ${Math.random()
        .toString(36)
        .substr(2, 9)}`,
    }
    await setSettings(apiKey, newSettings)
    const integration = await get(sirenaAppId)
    expect(integration.settings).toEqual({ ...settings, ...newSettings })
  })
  test('If settings value is not especified, should get the same settings', async () => {
    const {
      apiKey,
      settings: { sirenaAppId },
      settings,
    } = integrationTest
    await setSettings(apiKey)
    const integration = await get(sirenaAppId)
    expect(integration.settings).toEqual(settings)
  })
})
